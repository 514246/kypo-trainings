export interface RoleMapping {
  uagAdmin: string;
  trainingDesigner: string;
  trainingOrganizer: string;
  trainingTrainee: string;
  sandboxDesigner: string;
  sandboxOrganizer: string;
}
